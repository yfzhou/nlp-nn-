# models.py

import torch
import torch.nn as nn
from torch import optim
import numpy as np
import random
from sentiment_data import *

class MyNN(nn.Module):
    def __init__(self, inp, hid, out):
        super(MyNN, self).__init__()
        self.V = nn.Linear(inp, hid)
        self.g = nn.Tanh()
        self.W = nn.Linear(hid, out)
        self.log_softmax = nn.LogSoftmax(dim=0)
        # Initialize weights according to a formula due to Xavier Glorot.
        nn.init.xavier_uniform_(self.V.weight)
        nn.init.xavier_uniform_(self.W.weight)
    def forward(self, x):
        return self.log_softmax(self.W(self.g(self.V(x))))
def form_input(x) -> torch.Tensor:
    return torch.from_numpy(x).float()

class SentimentClassifier(object):
    """
    Sentiment classifier base type
    """

    def predict(self, ex: SentimentExample):
        """
        Makes a prediction on the given SentimentExample
        :param ex: example to predict
        :return: 0 or 1 with the label
        """
        raise Exception("Don't call me, call my subclasses")


class TrivialSentimentClassifier(SentimentClassifier):
    def predict(self, ex: SentimentExample):
        """
        :param ex:
        :return: 1, always predicts positive class
        """
        return 1


class NeuralSentimentClassifier(SentimentClassifier):
    """
    Implement your NeuralSentimentClassifier here. This should wrap an instance of the network with learned weights
    along with everything needed to run it on new data (word embeddings, etc.)
    """
    def __init__(self, ffnn, word_embeddings):
        self.ffnn = ffnn
        self.word_embeddings = word_embeddings
    def predict (self, ex: SentimentExample):
        x= np.zeros(50) 
        for j in range(len(ex.words)):
            x = np.add(x, np.array(self.word_embeddings.get_embedding(ex.words[j])))
        x = 1/(j+1) * x
        x = form_input(x)
        log_probs = self.ffnn.forward(x)
        prediction = torch.argmax(log_probs)
        return prediction
        


def train_deep_averaging_network(args, train_exs, dev_exs, word_embeddings)-> NeuralSentimentClassifier:
    """
    :param args: Command-line args so you can access them here
    :param train_exs: training examples
    :param dev_exs: development set, train_ys[idx]in case you wish to evaluate your model during training
    :param word_embeddings: set of loaded word embeddings
    :return: NeuralSentimentClassifier
    """
    # the highest so far is 300 10 lr = 0.0001 71%
    # 300 50 lr = 0.0002 71.8% 
    # 100 50 lr = 0.0002 71.3%
    feat_vec_size = word_embeddings.get_embedding_length()
    # Let's use 10 hidden units
    embedding_size = 100
    # We're using 2 classes. What's presented here is multi-class code that can scale to more classes, though
    # slightly more compact code for the binary case is possible.
    num_classes = 2

    # RUN TRAINING AND TEST
    num_epochs = 70
    ffnn = MyNN(feat_vec_size, embedding_size, num_classes)
    initial_learning_rate = 0.00001
    optimizer = optim.Adam(ffnn.parameters(), lr=initial_learning_rate)
    for epoch in range(0, num_epochs):
        ex_indices = [i for i in range(0, len(train_exs))]
        random.shuffle(ex_indices)
        total_loss = 0.0
        for idx in ex_indices:
            words = train_exs[idx].words
            # every sentence get a new x vector
            x= np.zeros(50) 
            # DAN
            for j in range(len(words)):
                x = np.add(x, np.array(word_embeddings.get_embedding(words[j])))
            x = 1/(j+1) * x
            x = form_input(x)
            y = train_exs[idx].label
            # Build one-hot representation of y. Instead of the label 0 or 1, y_onehot is either [0, 1] or [1, 0]. This
            # way we can take the dot product directly with a probability vector to get class probabilities.
            y_onehot = torch.zeros(num_classes)
            # scatter will write the value of 1 into the position of y_onehot given by y
            y_onehot.scatter_(0, torch.from_numpy(np.asarray(y,dtype=np.int64)), 1)
            # Zero out the gradients from the FFNN object. *THIS IS VERY IMPORTANT TO DO BEFORE CALLING BACKWARD()*
            ffnn.zero_grad()
            probs = ffnn.forward(x)
            # Can also use built-in NLLLoss as a shortcut here but we're being explicit here
            loss = torch.neg(probs).dot(y_onehot)
            total_loss += loss
            # Computes the gradient and takes the optimizer step
            loss.backward()
            optimizer.step()
        #print("Total loss on epoch %i: %f" % (epoch, total_loss))
    return NeuralSentimentClassifier(ffnn, word_embeddings)

